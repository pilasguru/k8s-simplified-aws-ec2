#!/bin/bash
# Scripts to destroy a kubernetes cluster into AWS using EC2 instances only

# NO CONFIGURATION REQUIRED
# it uses .env file to export variables

if [ -f $(dirname "$0")/.env ]; then
	. $(dirname "$0")/.env
	buck=$(echo $KOPS_STATE_STORE | cut -d '/' -f 3)
else 
	echo "ERROR: Do not exist "$(dirname "$0")"/.env"
	echo "... aborting ..." 
	exit 100
fi

kops delete cluster \
	--name ${KOPS_CLUSTER_NAME} \
	--yes

if [ $? -eq 0 ]; then  
	echo 
	echo "SUCCESS delete AWS cluster"
else
	echo
	echo "ERROR deleting AWS cluster, some resources may be still configured"
fi
echo

echo "Delete S3 bucket ..."
aws s3api delete-objects \
    --bucket ${buck} \
    --delete "$(aws s3api list-object-versions \
    			--bucket "${buck}" \
    			--output=json \
    			--query='{Objects: Versions[].{Key:Key,VersionId:VersionId}}')"

#aws s3api delete-objects \
#    --bucket ${buck} \
#    --delete "$(aws s3api list-object-versions \
#                    --bucket ${buck} \
#                    --output=json \
#                    --query='{Objects: DeleteMarkers[].{Key:Key,VersionId:VersionId}}')"

aws s3api list-object-versions --bucket ${buck} --output text | \
	grep "DELETEMARKERS" | while read OBJECTS
   		do
        	KEY=$( echo $OBJECTS| awk '{print $3}')
        	VERSION_ID=$( echo $OBJECTS | awk '{print $5}')
        	#echo $KEY
        	#echo $VERSION_ID
        	aws s3api delete-object --bucket ${buck} --key $KEY --version-id $VERSION_ID > /dev/null
    	done

aws s3 rm ${KOPS_STATE_STORE} --recursive

aws s3api delete-bucket --bucket ${buck}

#echo "Delete IAM roles ..."   # DEPRECATED by kops !!
#for i in nodes master; do 
#	aws iam remove-role-from-instance-profile \
#		--instance-profile-name $i.${KOPS_CLUSTER_NAME} \
#		--role-name $i.${KOPS_CLUSTER_NAME}
#	aws iam delete-role-policy \
#		--role-name $i.${KOPS_CLUSTER_NAME} \
#		--policy-name $i.${KOPS_CLUSTER_NAME}
#	aws iam delete-role \
#		--role-name $i.${KOPS_CLUSTER_NAME}
#done

test -f $(dirname "$0")/.env && rm $(dirname "$0")/.env
test -f $(dirname "$0")/.bucket_name.dat && rm $(dirname "$0")/.bucket_name.dat
echo "Done!"
exit 0


