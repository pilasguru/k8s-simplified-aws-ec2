#!/bin/bash
# Scripts to setup a kubernetes cluster into AWS using EC2 instances only

### CONFIGURE THESE VARIABLES
export AWS_PROFILE=awesome-profile
export KOPS_CLUSTER_NAME=k.awesome.club
export AWS_DEFAULT_REGION=us-east-1
master_size=t2.micro
node_size=t2.medium
master_coun=3

# History:
# 2020-04-18: 
# - AWS_DEFAULT_REGION to setup different region from profile
# - Add LocationConstraint to create S3

# check required commands
aws --version > /dev/null 2>&1
if [ $? -ne 0 ]; then echo "ERROR: 'aws' command is not installed"; exit 50; fi
kops version > /dev/null 2>&1
if [ $? -ne 0 ]; then echo "ERROR: 'kops' command is not installed"; exit 50; fi
kubectl version --client=true > /dev/null 2>&1
if [ $? -ne 0 ]; then echo "ERROR: 'kubectl' command is not installed"; exit 50; fi

# check AWS access configuration
aws ec2 describe-regions > /dev/null 2>&1
if [ $? -ne 0 ]; then echo "ERROR: AWS configuration is incorrect"; exit 60; fi
aws route53 list-hosted-zones --query='{Objects: HostedZones[].{Name:Name}}' | grep ${KOPS_CLUSTER_NAME} > /dev/null 2>&1
if [ $? -ne 0 ]; then echo "ERROR: Domain ${KOPS_CLUSTER_NAME} is not configured as hosted zone into Route53"; exit 70; fi

# check previous setup
if [ ! -f $(dirname "$0")/.bucket_name.dat ]; then 
	name_id=$(date +%s)
	bucket_name=k8s-state-store-${name_id}
	export KOPS_STATE_STORE=s3://${bucket_name}
	echo s3://$bucket_name > .bucket_name.dat

	aws s3api create-bucket \
		--bucket ${bucket_name} \
		--create-bucket-configuration LocationConstraint=${AWS_DEFAULT_REGION}

	aws s3api put-bucket-versioning \
		--bucket ${bucket_name} \
		--versioning-configuration Status=Enabled

	aws s3api put-bucket-encryption \
		--bucket ${bucket_name} \
		--server-side-encryption-configuration '{"Rules":[{"ApplyServerSideEncryptionByDefault":{"SSEAlgorithm":"AES256"}}]}'
else
	export KOPS_STATE_STORE=$(cat bucket_name.dat)
fi

# create cluster
kops create cluster \
	--zones ${AWS_DEFAULT_REGION}a,${AWS_DEFAULT_REGION}b \
	--dns-zone=${KOPS_CLUSTER_NAME} \
	--topology private \
	--networking calico \
	--master-size $master_size \
	--master-count $master_coun \
	--node-size $node_size \
	--name ${KOPS_CLUSTER_NAME} \
	--yes

kops create secret --name ${KOPS_CLUSTER_NAME} sshpublickey admin -i ~/.ssh/id_rsa.pub

echo "export KOPS_STATE_STORE=$(cat .bucket_name.dat)" > .env
echo "export KOPS_CLUSTER_NAME=${KOPS_CLUSTER_NAME}" >> .env
echo "export AWS_PROFILE=${AWS_PROFILE}" >> .env
echo "export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" >> .env
echo "EXCECUTE: source .env"
echo
echo "Wait 5 minutes and execute"
echo "source .env && kops validate cluster" 
echo "and repeat until receive message"
echo "Your cluster ${KOPS_CLUSTER_NAME} is ready"
echo
sleep 3

exit 0
