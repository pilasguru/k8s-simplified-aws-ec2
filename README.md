# K8S simplified with AWS EC2

Scripts to setup and destroy a kubernetes cluster into AWS using EC2 instances only.

**This project do not use EKS**

## Requirements

### Tools 

These CLI commands must be propertly installed:

* aws 
* kops
* kubectl

### AWS requisites

 **IAM user**  

* Programatic access (ACCESS_KEY and SECRET_ACCESS_KEY) configured with a profile
* Required policies: AmazonEC2FullAccess - AmazonS3FullAccess - AmazonVPCFullAccess - AmazonRoute53FullAccess
* Optional policy: AdministratorAccess or other. 


## Configuration

Edit `setup.sh` and configure the environment variables at the first lines to meet your requirements/configuration.


## Use

### Setup

```
./setup.sh
```

will create all required resources and two files into the local folder:

1. `.bucket_name.dat` with created bucket to storage kops state config

```
$ cat .bucket_name.dat
s3://k8s-state-store-1582484379
```

2. `.env` with required export environment varialbes to use `kubectl` and `aws` tools

```
$ cat .env
export KOPS_STATE_STORE=s3://k8s-state-store-1582484379
export KOPS_CLUSTER_NAME=k.awesome.club
export AWS_PROFILE=awesome-profile
```

### Destroy

```
./destroy.sh
```

will destroy complete cluster and resources.  Also will clean local folder files created by setup.sh.

## Similar project

My project [KubernetesAWS](https://gitlab.com/pilasguru/KubernetesAWS) was my first approach to this infraestructure, but create a VAGRANT VM where all configuration and requirements are authomaticaly created.

## License

[MIT License](LICENSE)
